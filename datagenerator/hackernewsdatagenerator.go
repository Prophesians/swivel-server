package datagenerator

import (
	"time"
	"gitlab.com/Prophesians/swivel-server/crawlers"
	"fmt"
	"gitlab.com/Prophesians/swivel-server/repositories"
)

func UpdateTopNews(esURL string,topNewsIDList []int, batchSize int) error {
	apiCallFrequency := 1 * time.Second
	if len(topNewsIDList) < batchSize {
		articles, err := crawlers.GetBatchArticles(apiCallFrequency, topNewsIDList, len(topNewsIDList)-1)
		if err != nil {
			fmt.Println("Error while making a call for batch articles")
			return err
		}
		repositories.IndexArticles(esURL, articles)
		return nil
	}
	articles, err := crawlers.GetBatchArticles(apiCallFrequency, topNewsIDList, batchSize)
	if err != nil {
		fmt.Println("Error while making a call for batch articles")
		return err
	}
	repositories.IndexArticles(esURL, articles)
	UpdateTopNews(esURL, topNewsIDList[batchSize:], batchSize)
	return nil
}

func MakeCall(batchSize int, esURL string){
	topArticlesList, err := crawlers.GetTopNewsList()
	if err != nil {
		fmt.Println("Error while calling hacker news", err)
	}
	fmt.Println("\n Got Articles : ", len(topArticlesList))
	err = UpdateTopNews(esURL, topArticlesList, batchSize)
	if err != nil {
		fmt.Println("Error while updating the articels ", err)
	}
}

func Generate(duration time.Duration, batchSize int, esURL string){
	ticker := time.NewTicker(duration)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <- ticker.C:
				MakeCall(batchSize, esURL)
			case <- quit:
				ticker.Stop()
				return
			}
		}
	}()
}
