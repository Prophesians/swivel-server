-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE users(
  user_id serial PRIMARY KEY,
  favorite_tags text[],
  recommended_tags text[]
);
-- +goose Down
DROP TABLE swivelserver.users;
-- SQL in this section is executed when the migration is rolled back.
