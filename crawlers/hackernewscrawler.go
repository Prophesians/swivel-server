package crawlers

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"gopkg.in/jdkato/prose.v2"
	"log"
	"time"
)

type Article struct {
	By    string
	Id    int
	Score int
	Time  int
	Title string
	Url   string
	Tags  []string
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func getNewsDetails(newsID int) (*Article, error) {
	singleNews := &Article{}
	urlString := `https://hacker-news.firebaseio.com/v0/item/%d.json?print=pretty`
	newsResp, err := http.Get(fmt.Sprintf(urlString, newsID))
	if err != nil {
		fmt.Println("Eror while calling hacker news API", err)
		return nil, err
	}
	defer newsResp.Body.Close()
	news_byte, err := ioutil.ReadAll(newsResp.Body)
	if err != nil {
		fmt.Println("Eror while calling hacker news API", err)
		return nil, err
	}
	err = json.Unmarshal(news_byte, singleNews)
	if err != nil {
		fmt.Println("Error in unmarshaling the news response", err)
		return nil, err
	}
	doc, err := prose.NewDocument(singleNews.Title)
	if err != nil {
		log.Fatal(err)
	}

	for _, tok := range doc.Tokens() {
		if tok.Tag == "NN" || tok.Tag == "NNP" || tok.Tag == "NNPS" || tok.Tag == "NNS" {
			if !contains(singleNews.Tags, tok.Text) {
				singleNews.Tags = append(singleNews.Tags, tok.Text)
			}
		}
	}

	return singleNews, nil
}

func makeCall(newsID, newsIndex int, articleChannel []*Article) error{
	singleNews, err := getNewsDetails(newsID)
	if err != nil {
		fmt.Println("error while getting single article",err)
		return err
	}
	articleChannel[newsIndex] = singleNews
	return nil
}

func GetTopNewsList() ([]int, error) {
	fmt.Println("\n Making a call for getting top news", time.Now())
	resp, err := http.Get(`https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`)
	if err != nil {
		fmt.Println("Eror while calling hacker news API", err)
		return nil, err
	}
	defer resp.Body.Close()
	bodyByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Eror while reading the resp body of top news list api", err)
		return nil, err
	}
	var topNewsIDList []int
	err = json.Unmarshal(bodyByte, &topNewsIDList)
	if err != nil {
		fmt.Println("Eror while unmarshaling the top news ID list API response", err)
		return nil, err
	}
	totalNews := len(topNewsIDList)
	fmt.Println("\n Total top news are : ", totalNews)
	return topNewsIDList, nil
}

func GetBatchArticles(apiCallFrequency time.Duration,topNewsIDList []int,batchSize int) ([]*Article, error) {
	articles := make([]*Article, batchSize)
	for newsIndex, newsID := range topNewsIDList[0:batchSize] {
		fmt.Print("\n Making a call after ", apiCallFrequency, " for each internal news ", time.Now())
		time.Sleep(apiCallFrequency)
		err := makeCall(newsID, newsIndex, articles)
		if err != nil {
			fmt.Println("error while making a call for single article:", err)
			return nil, err
		}
	}
	return articles, nil
}
