package crawlers

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/jarcoal/httpmock"
	"strings"
	"time"
)

func TestGetTopNewsSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty",
		httpmock.NewStringResponder(200, `[1,2]`))

	newsList, err := GetTopNewsList()
	expectedList := []int{1,2}

	assert.Nil(t, err)
	assert.Equal(t, expectedList, newsList)
}

func TestGetTopNewsFailureForTheTopNewsCall(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	newsList, err := GetTopNewsList()

	assert.NotNil(t, err)
	assert.Nil(t, newsList)
	assert.True(t, strings.Contains(err.Error(), ` https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`))
}

func TestGetTopNewsFailureForUnmarshalingTheTopNewsApiResponse(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty",
		httpmock.NewStringResponder(200, `[`))

	newsList, err := GetTopNewsList()

	assert.NotNil(t, err)
	assert.Nil(t, newsList)
	assert.True(t, strings.Contains(err.Error(), `unexpected end of JSON input`))
}

func TestGetBatchArticlesSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://hacker-news.firebaseio.com/v0/item/1.json?print=pretty",
		httpmock.NewStringResponder(200, `{"title": "some"}`))

	httpmock.RegisterResponder("GET", "https://hacker-news.firebaseio.com/v0/item/2.json?print=pretty",
		httpmock.NewStringResponder(200, `{"title": "something"}`))

	newsList, err := GetBatchArticles(1 * time.Second, []int {1, 2}, 2)
	expectedList := []*Article{
		{
			Title: "some",
		},
		{
			Title: "something",
			Tags: []string{"something"},
		},
	}

	assert.Nil(t, err)
	assert.Equal(t, expectedList, newsList)
}

func TestGetBatchArticlesFailure(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://hacker-news.firebaseio.com/v0/item/1.json?print=pretty",
		httpmock.NewStringResponder(500, `{"error": "some"}`))

	newsList, err := GetBatchArticles(1 * time.Second, []int {1, 2}, 2)

	assert.NotNil(t, err)
	assert.Nil(t, newsList)
}