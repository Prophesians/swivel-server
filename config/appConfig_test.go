package config

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestGetConfig(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, 8080, config.Port)
	assert.Equal(t, "localhost", config.DBHost)
	assert.Equal(t, "dbname", config.DBName)
	assert.Equal(t, "user", config.DBUser)
	assert.Equal(t, "pass", config.DBPassword)
	assert.Equal(t, "schema", config.Search_Path)
	assert.Equal(t, 6432, config.DBPort)
}

func TestGetConfigFailure(t *testing.T) {
	_, err := GetConfig("./wrong", true)
	assert.NotNil(t, err)
}

func TestAppConfig_GetDBName(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, "dbname", config.GetDBName())
}

func TestAppConfig_GetDBHost(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, "localhost", config.GetDBHost())
}

func TestAppConfig_GetDBPassword(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, "pass", config.GetDBPassword())
}

func TestAppConfig_GetDBPort(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, 6432, config.GetDBPort())
}

func TestAppConfig_GetDBUser(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, "user", config.GetDBUser())
}

func TestAppConfig_GetPort(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, 8080, config.GetPort())
}

func TestAppConfig_GetSearch_Path(t *testing.T) {
	config, err := GetConfig("./config.test.json", true)
	assert.Nil(t, err)
	assert.Equal(t, "schema", config.GetSearch_Path())
}