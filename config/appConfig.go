package config

import (
	"github.com/tkanos/gonfig"
	"fmt"
	"os"
	"strconv"
)

type AppConfig struct {
	Port int
	DBHost string
	DBName string
	DBUser string
	DBPassword string
	Search_Path string
	ESURL string
	DBPort int
}

func GetConfig(filePath string, devMode bool) (*AppConfig, error) {
	appConfig := AppConfig{}
	var err error
	if devMode{
		err := gonfig.GetConf(filePath, &appConfig)
		if err != nil {
			fmt.Println("error while reading the config file", err)
			return nil, err
		}
	}else{
		port, _ := strconv.Atoi(os.Getenv("PORT"))
		dbPort, _ := strconv.Atoi(os.Getenv("DBPORT"))
		appConfig = AppConfig{
			Port:port,
			DBHost: os.Getenv("DBHOST"),
			DBName: os.Getenv("DBNAME"),
			DBUser: os.Getenv("DBUSER"),
			DBPassword: os.Getenv("DBPASSWORD"),
			Search_Path: os.Getenv("SEARCHPATH"),
			ESURL: os.Getenv("ESURL"),
			DBPort: dbPort,
		}
	}

	return &appConfig, err
}

func (a *AppConfig) GetPort() (int) {
	return a.Port
}

func (a *AppConfig) GetDBHost() (string) {
	return a.DBHost
}

func (a *AppConfig) GetDBName() (string) {
	return a.DBName
}

func (a *AppConfig) GetDBUser() (string) {
	return a.DBUser
}

func (a *AppConfig) GetDBPassword() (string) {
	return a.DBPassword
}

func (a *AppConfig) GetSearch_Path() (string) {
	return a.Search_Path
}

func (a *AppConfig) GetDBPort() (int) {
	return a.DBPort
}

func (a *AppConfig) GetESURL() (string) {
	return a.ESURL
}
