package repositories

import (
	"gitlab.com/Prophesians/swivel-server/crawlers"
	"github.com/olivere/elastic"
	"fmt"
	"golang.org/x/net/context"
	"strconv"
	"encoding/json"
)

//todo: create only one ES client

func Ping(url string) error {
	client, err := elastic.NewClient(elastic.SetSniff(false),elastic.SetURL(url))
	if err != nil {
		fmt.Println("error while creating client", err)
		return err
	}
	_, _, err = client.Ping(url).Do(context.Background())
	return err
}

func IndexArticles(esURL string,articles []*crawlers.Article) {
	client, err := elastic.NewClient(elastic.SetSniff(false),elastic.SetURL(esURL))
	if err != nil {
		fmt.Println("error while creating client", err)
	}

	bulkRequest := client.Bulk()

	for _, article := range articles {
		indexReq := elastic.NewBulkIndexRequest().Index("swivel").Type("swivel").Id(strconv.Itoa(article.Id)).Doc(article)
		bulkRequest = bulkRequest.Add(indexReq)
	}
	bulkResponse, err := bulkRequest.Do(context.Background())
	if err != nil {
		fmt.Println("Error whiule doing bulk index", err)
	}
	indexed := bulkResponse.Indexed()
	if len(indexed) != len(articles) {
		fmt.Println("all articles was not indexed")
	}
}

func GetArticles(esURL string,tags []string, from, to int) ([]crawlers.Article, error) {
	var articles []crawlers.Article
	client, err := elastic.NewClient(elastic.SetSniff(false),elastic.SetURL(esURL))
	if err != nil {
		fmt.Println("error while creating client", err)
		return articles, err
	}

	termQuery := elastic.NewBoolQuery()

	for _, tag := range tags {
		query := elastic.NewMatchQuery("Tags", tag)
		termQuery.Should(query)
	}

	searchResult, err := client.
		Search().
		Index("swivel").
		Query(termQuery).
		From(from).
		Size(to).
		Do(context.Background())

	if err != nil {
		fmt.Println("Error while making search request", err)
		return articles, err
	}

	if searchResult.Hits.TotalHits > 0 {
		for _, hit := range searchResult.Hits.Hits {
			var article crawlers.Article
			err := json.Unmarshal(*hit.Source, &article)
			if err != nil {
				fmt.Println("error while unmarshaling", err)
			}
			articles = append(articles, article)

		}
	} else {
		fmt.Print("Found no articles\n")
		return articles, nil
	}

	return articles, nil

}
