package models

import (
	"log"
	"gopkg.in/jdkato/prose.v2"
	"strings"
)

func GetToken(historyTopic string) []string {
	var tokens []string

	splitedTopics := strings.Split(historyTopic, "-")

	if len(splitedTopics) > 1 {
		historyTopic = strings.Join(splitedTopics[0:len(splitedTopics)-1], " ")
	}

	doc, err := prose.NewDocument(historyTopic)
	if err != nil {
		log.Fatal(err)
	}

	for _, tok := range doc.Tokens() {
		if tok.Tag == "NN"&&
			!strings.Contains(tok.Text,"/") &&
			!strings.Contains(tok.Text,"@") &&
			len(tok.Text) > 1{
			tokens = append(tokens, tok.Text)
		}
	}
	return tokens
}

func GetTokens(historyTopic []string) []string {
	var tokens []string
	for _, URL := range historyTopic {
		tokens = append(tokens, GetToken(URL)...)
	}
	return tokens
}
