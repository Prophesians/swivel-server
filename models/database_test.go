package models

import (
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"errors"
)

func TestTags_Insert_success(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	columns := []string{"user_id"}
	givenTags := []string{"computer"}

	mock.ExpectExec("INSERT INTO users").WithArgs(pq.Array(givenTags)).WillReturnResult(sqlmock.NewResult(1,1))
	mock.ExpectQuery("SELECT user_id FROM users ORDER by user_id DESC LIMIT 1").WithArgs().WillReturnRows(sqlmock.NewRows(columns).AddRow(4))

	tags := Tags{}

	requestBody := RequestBody{Tags: givenTags}

	resultUserID, err := tags.Insert(db, requestBody)
	if err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}

	assert.Equal(t, int64(4), resultUserID)
}

func TestTags_Insert_Failure_For_Insert_Query(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	givenTags := []string{"computer"}

	mock.ExpectExec("INSERT INTO users").WithArgs(pq.Array(givenTags)).WillReturnError(errors.New("can't upload"))

	tags := Tags{}

	requestBody := RequestBody{Tags: givenTags}

	_, err = tags.Insert(db, requestBody)

	assert.NotNil(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestTags_Insert_Failure_For_Select_Query(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	givenTags := []string{"computer"}

	mock.ExpectExec("INSERT INTO users").WithArgs(pq.Array(givenTags)).WillReturnResult(sqlmock.NewResult(1,1))
	mock.ExpectQuery("SELECT user_id FROM users ORDER by user_id DESC LIMIT 1").WithArgs().WillReturnError(errors.New("some error"))

	tags := Tags{}

	requestBody := RequestBody{Tags: givenTags}

	_, err = tags.Insert(db, requestBody)

	assert.NotNil(t, err)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

