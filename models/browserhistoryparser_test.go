package models

import (
	"testing"
	"fmt"
)

func TestGetTokens(t *testing.T) {
	tokens := GetTokens([]string{
		"Download Latest MP3 Songs Online: Play Old & New MP3 Music Online Free on Gaana.com",
		"Window setInterval() Method",
		"javascript setinterval - Google Search",
		"js timmer - Google Search",
		"javascript - How to get browsing history using history API in Chrome extension - Stack Overflow",
		"browser history chrome extension - Google Search",
		"developer.chorme - Google Search",
	})
	fmt.Println("_______", tokens)
}
