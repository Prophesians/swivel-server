package models

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq"
)

type RequestBody struct {
	Tags []string
}

type HistoryBody struct {
	Topics []string
}

type Database interface {
	Insert(db *sql.DB, body RequestBody) (int64, error)
	UpdateTags(db *sql.DB, tags []string, userID string) error
	SearchTags(db *sql.DB, userID string) ([]string, error)
}

type Tags struct {
}

func (t *Tags) UpdateTags(db *sql.DB, giventTags []string, userID string) error {
	previousTags, err := t.SearchTags(db, userID)

	if err != nil {
		fmt.Println("Error while saving recommended_tags to DB: getting previous saved tags", err)
		return err
	}

	allTag := append(previousTags, giventTags...)

	uniqueTags := GetUniques(allTag)

	updateQueryString := "UPDATE users SET favorite_tags = $1 WHERE user_id=$2;"
	_, err = db.Exec(updateQueryString, pq.Array(uniqueTags), userID)
	if err != nil {
		fmt.Println("Error while saving recommended_tags to DB", err)
		return err
	}
	return nil
}

func (t *Tags) Insert(db *sql.DB, body RequestBody) (int64, error) {
	insertQueryString := "INSERT INTO users (favorite_tags) values ($1)"
	_, err := db.Exec(insertQueryString, pq.Array(body.Tags))
	if err != nil {
		fmt.Println("Error while saving favorite_tags to DB", err)
		return 0, err
	}
	selectQueryString := `SELECT user_id FROM users ORDER by user_id DESC LIMIT 1`
	var user_id int64
	err = db.QueryRow(selectQueryString).Scan(&user_id)
	if err != nil {
		fmt.Println("Error while getting user ID from DB", err)
		return 0, err
	}
	return user_id, nil
}

func (t *Tags) SearchTags(db *sql.DB, userID string) ([]string, error) {
	selectQueryString := `SELECT favorite_tags FROM users WHERE user_id=$1`
	tags := []string{}
	err := db.QueryRow(selectQueryString, userID).Scan(pq.Array(&tags))
	if err != nil {
		fmt.Println("Error while searching tags from DB", err)
		return []string{}, err
	}
	return tags, nil
}

func GetUniques(input []string) []string {
	u := make([]string, 0, len(input))
	m := make(map[string]bool)

	for _, val := range input {
		if _, ok := m[val]; !ok {
			m[val] = true
			u = append(u, val)
		}
	}

	return u
}