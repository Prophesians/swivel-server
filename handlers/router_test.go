package handlers

import (
	"testing"
	"net/http/httptest"
	"net/http"
	"gitlab.com/Prophesians/swivel-server/context"
	m "gitlab.com/Prophesians/swivel-server/mocks"
	"fmt"
	"gitlab.com/Prophesians/swivel-server/models"
)

func okHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.WriteHeader(200)
		fmt.Fprintf(w, "I am alive")
	}
}

func TestRouter(t *testing.T) {
	req, err := http.NewRequest("GET", "/health-check", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	appContext := &context.AppContext{}

	mockHandler := &m.Handler{}
	databaseModel := &models.Tags{}
	mockHandler.On("IsAlive").Return(okHandler())
	mockHandler.On("GetArticles", appContext, databaseModel).Return(okHandler())
	mockHandler.On("SaveTags", appContext, databaseModel).Return(okHandler())
	mockHandler.On("SaveHistory", appContext, databaseModel).Return(okHandler())
	handler := Router(appContext, mockHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	mockHandler.AssertExpectations(t)

}
