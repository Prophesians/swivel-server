package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/Prophesians/swivel-server/context"
	"gitlab.com/Prophesians/swivel-server/crawlers"
	"gitlab.com/Prophesians/swivel-server/models"
	"gitlab.com/Prophesians/swivel-server/repositories"
	"io/ioutil"
	"net/http"
	"strconv"
)

type Handler interface {
	IsAlive() http.HandlerFunc
	GetArticles(context *context.AppContext, model models.Database) http.HandlerFunc
	SaveTags(context *context.AppContext, model models.Database) http.HandlerFunc
	SaveHistory(context *context.AppContext, model models.Database) http.HandlerFunc
}

type SwivelServerHandler struct {
}

type articleResponse struct {
	Topics []crawlers.Article
}

type saveTagsResponse struct {
	UserID int64
}

func (ssh *SwivelServerHandler) IsAlive() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.WriteHeader(200)
		fmt.Fprintf(w, "I am alive")
	}
}

func (ssh *SwivelServerHandler) GetArticles(context *context.AppContext, model models.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		userID := vars["user_id"]
		noOfArticlesRaw := vars["no_of_articles"]
		noOfArticles, err := strconv.Atoi(noOfArticlesRaw)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		usertags, err := model.SearchTags(context.Repository.DB, userID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		articles, err := repositories.GetArticles(context.AppConfig.ESURL, usertags, 0, noOfArticles)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		js, err := json.Marshal(articleResponse{articles})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

func (ssh *SwivelServerHandler) SaveTags(context *context.AppContext, model models.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		var requestData models.RequestBody
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Println("Error while reading request body", err)
			http.Error(w, "Service error", http.StatusInternalServerError)
			return
		}
		err = json.Unmarshal(body, &requestData)
		if err != nil {
			fmt.Println("Error while unmarshaling request body", err)
			http.Error(w, "Service error", http.StatusInternalServerError)
			return
		}
		user_id, err := model.Insert(context.Repository.DB, requestData)
		if err != nil {
			fmt.Println("Error while saving the tags to Database", err)
			http.Error(w, "Service error", http.StatusInternalServerError)
			return
		}

		response := saveTagsResponse{
			UserID: user_id,
		}
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(js)
	}
}

func (ssh *SwivelServerHandler) SaveHistory(context *context.AppContext, model models.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		var historyData models.HistoryBody
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Println("Error while reading request body", err)
			http.Error(w, "Service error", http.StatusInternalServerError)
			return
		}
		err = json.Unmarshal(body, &historyData)
		if err != nil {
			fmt.Println("Error while unmarshaling request body", err)
			http.Error(w, "Service error", http.StatusInternalServerError)
			return
		}

		vars := mux.Vars(r)
		userID := vars["user_id"]
		go func() {
			tags := models.GetTokens(historyData.Topics)

			err = model.UpdateTags(context.Repository.DB, tags, userID)
			if err != nil {
				fmt.Println("Error while saving the tags to Database", err)
				return
			}
		}()

		w.WriteHeader(200)
		return
	}
}
