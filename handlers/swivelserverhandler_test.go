package handlers

import (
	"net/http/httptest"
	"testing"
	"net/http"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"encoding/json"
	"bytes"
	"gitlab.com/Prophesians/swivel-server/models"
	"gitlab.com/Prophesians/swivel-server/config"
	"gitlab.com/Prophesians/swivel-server/repositories"
	context2 "gitlab.com/Prophesians/swivel-server/context"
	"gitlab.com/Prophesians/swivel-server/mocks"
	"errors"
)

func TestSwivelServerHandler_IsAlive(t *testing.T) {
	req, err := http.NewRequest("GET", "/health-check", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	swivelServerHandler := SwivelServerHandler{}
	handler := http.HandlerFunc(swivelServerHandler.IsAlive())

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `I am alive`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

//func TestSwivelServerHandler_GetArticles(t *testing.T) {
//	db, _, err := sqlmock.New()
//	if err != nil {
//		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
//	}
//	defer db.Close()
//
//	req, err := http.NewRequest("GET", "/api/2/articles", nil)
//	if err != nil {
//		t.Fatal(err)
//	}
//
//	rr := httptest.NewRecorder()
//
//	context := context2.AppContext{
//		AppConfig:  &config.AppConfig{},
//		Repository: &repositories.Repository{db},
//	}
//
//	swivelServerHandler := SwivelServerHandler{}
//	handler := http.HandlerFunc(swivelServerHandler.GetArticles())
//
//	handler.ServeHTTP(rr, req)
//
//	if status := rr.Code; status != http.StatusOK {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusOK)
//	}
//
//	expected := `{"Topics":[{"Header":"'Your Time On Facebook may soon be monitored","Summary":"Facebook has reportedly introduced a new feature'`
//	if strings.Contains(rr.Body.String(), expected) {
//		t.Errorf("handler returned unexpected body: got %v want %v",
//			rr.Body.String(), expected)
//	}
//}

func TestSwivelServerHandler_SaveTags_Success(t *testing.T) {
	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	tags := []string{"computer"}
	requestBody := models.RequestBody{
		Tags:   tags,
	}

	data, err := json.Marshal(requestBody)

	req, err := http.NewRequest("POST", "/api/tags", bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	swivelServerHandler := SwivelServerHandler{}
	context := context2.AppContext{
		AppConfig:  &config.AppConfig{},
		Repository: &repositories.Repository{db},
	}

	mockModel := &mocks.Database{}

	mockModel.On("Insert",db,requestBody).Return(int64(1), nil)

	handler := http.HandlerFunc(swivelServerHandler.SaveTags(&context, mockModel))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	mockModel.AssertExpectations(t)
}

func TestSwivelServerHandler_SaveTags_Failure(t *testing.T) {
	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	tags := []string{"computer"}
	requestBody := models.RequestBody{
		Tags:   tags,
	}

	data, err := json.Marshal(requestBody)

	req, err := http.NewRequest("POST", "/api/tags", bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	swivelServerHandler := SwivelServerHandler{}
	context := context2.AppContext{
		AppConfig:  &config.AppConfig{},
		Repository: &repositories.Repository{db},
	}

	mockModel := &mocks.Database{}

	mockModel.On("Insert",db,requestBody).Return(int64(0), errors.New("db error"))

	handler := http.HandlerFunc(swivelServerHandler.SaveTags(&context, mockModel))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	mockModel.AssertExpectations(t)
}
