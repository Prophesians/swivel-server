package handlers

import (
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/Prophesians/swivel-server/context"
	"gitlab.com/Prophesians/swivel-server/models"
	"github.com/rs/cors"
)

func Router(appContext *context.AppContext, handler Handler) http.Handler {
	router := mux.NewRouter()

	databaseModel := &models.Tags{}
	router.HandleFunc("/health-check", handler.IsAlive()).Methods("GET")
	router.HandleFunc("/api/{user_id}/articles/{no_of_articles}", handler.GetArticles(appContext, databaseModel)).Methods("GET")
	router.HandleFunc("/api/tags", handler.SaveTags(appContext, databaseModel)).Methods("POST")
	router.HandleFunc("/api/{user_id}/history", handler.SaveHistory(appContext, databaseModel)).Methods("POST")

	return cors.Default().Handler(router)
}