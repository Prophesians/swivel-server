package scrappers

import (
	"fmt"
	"net/http"
	"golang.org/x/net/html"
	"log"
	"strings"
)

func GetArticleBody(articleURL string) (string, error) {
	newsResp, err := http.Get(articleURL)
	if err != nil {
		fmt.Println("Eror while calling hacker news API", err)
		return "", err
	}
	defer newsResp.Body.Close()

	if err != nil {
		log.Fatal(err)
	}
	data := ""
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.TextNode {
			if !strings.Contains(n.Data, "window.") &&
				!strings.Contains(n.Data, `"height":` ) &&
				!strings.Contains(n.Data, `style=` ) &&
				!strings.Contains(n.Data, `/*<![CDATA[*/` ) &&
				!strings.Contains(n.Data, `<![CDATA` ) &&
				!strings.Contains(n.Data, `href="` ) &&
				!strings.Contains(n.Data, `();` ) &&
				!strings.Contains(n.Data, `{display:none;}  ` ) &&
				!strings.Contains(n.Data, `:"http:` ) &&
				!strings.Contains(n.Data, `!important;` ) &&
				!strings.Contains(n.Data, `var ` ) &&
				!strings.Contains(n.Data, `function(statusbar)` ) &&
				!strings.Contains(n.Data, `!==` ) &&
				n.Data != ` ` &&
				len(strings.Split(n.Data, " ")) > 5 &&
				!strings.Contains(n.Data, ` = ` ) {
				currentData := strings.TrimSpace(n.Data)
				data = data + " " + currentData
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	doc, err := html.Parse(newsResp.Body)

	f(doc)

	trimmedData := strings.TrimSpace(data)
	slittedData := strings.Split(trimmedData, ". ")
	filteredData := strings.Join(slittedData, "\n")

	//bag := tldr.New()
	//result, err := bag.Summarize(filteredData, 5)
	//fmt.Println("error :", err)

	//result, err = bag.Summarize(result, 5)
	//fmt.Println("error :", err)

	fmt.Println(filteredData)
	return "", nil
}