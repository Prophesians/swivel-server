
## Steps to setup DB

- Login using postgres user in postgres DB


        create database swivel;

        create user swivel-dev with password 'swivel-dev';

        alter user swiveldev with password 'swiveldev';

- Login using postgres user in swivel DB

        create schema swivelserver;

        grant ALL ON SCHEMA swivelserver TO swiveldev ;