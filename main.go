package swivel_server

import (
	"github.com/urfave/negroni"
	"net/http"
	"gitlab.com/Prophesians/swivel-server/handlers"
	"gitlab.com/Prophesians/swivel-server/config"
	"flag"
	"log"
	"strconv"
	"gitlab.com/Prophesians/swivel-server/repositories"
	"gitlab.com/Prophesians/swivel-server/context"
	"gitlab.com/Prophesians/swivel-server/datagenerator"
	"fmt"
	"time"
)

func main() {
	configPath := flag.String("configPath", "./config/config.development.json", "provide the config path")
	devMode := flag.Bool("devmode", false, "")
	if *configPath == "" {
		log.Fatal("Config file path should be provided")
	}
	appConfig, err := config.GetConfig(*configPath, *devMode)
	fmt.Print("Starting the app on port : ", appConfig.GetPort(), "\n")
	if err != nil {
		log.Fatal("error generating config")
	}
	repository, err := repositories.GetRepository(appConfig)
	if err != nil {
		log.Fatal("Error :", err)
	}
	defer repository.DB.Close()
	err = repository.Ping()
	if err != nil {
		log.Fatal("Error :", err)
	}

	err = repositories.Ping(appConfig.ESURL)

	if err != nil {
		log.Fatal("Error while connecting to es")
	}

	duration := 7200 * time.Second
	batchSize := 30
	go datagenerator.MakeCall(batchSize, appConfig.ESURL)
	go datagenerator.Generate(duration, batchSize, appConfig.ESURL)

	appContext := &context.AppContext{
		AppConfig:  appConfig,
		Repository: &repository,
	}

	n := negroni.Classic()
	swivelServerHandler := &handlers.SwivelServerHandler{}
	n.UseHandler(handlers.Router(appContext, swivelServerHandler))
	http.ListenAndServe(":"+strconv.Itoa(appConfig.GetPort()), n)
}
